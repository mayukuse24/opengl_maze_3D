#include "player.h"

void Hero::DrawObject(glm::mat4 VP, struct GLMatrices Matrices)
{
  glUseProgram(programID);
  Matrices.model = glm::mat4(1.0f);
  glm::mat4 MVP;
  glm::mat4 translateHero = glm::translate (glm::vec3(this->x, this->y, this->z)); // glTranslatef
  glm::mat4 rotateHero = glm::rotate((float)(this->rotation_angle*M_PI/180.0f), glm::vec3(0,1,0));
  glm::mat4 HeroTransform = translateHero * rotateHero;
  Matrices.model *= HeroTransform;
  MVP = VP * Matrices.model; // MVP = p * V * M
  glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
  draw3DObject(this->obj);
}

void Hero::Restart()
{
  this->x=0;
  this->z=0;
  this->y=35;
  this->rotation_angle=0;
  this->vy = 0;
}
