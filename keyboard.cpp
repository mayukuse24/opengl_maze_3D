#include "shape.h"
#include "keyboard.h"
#include "cube.h"
#include "player.h"
#include "game.h"

#include <iostream>

using namespace std;

int fc;

void quit(GLFWwindow *window)
{
  glfwDestroyWindow(window);
  glfwTerminate();
  exit(EXIT_SUCCESS);
}

void keyboard (GLFWwindow* window, int key, int scancode, int action, int mods)
{
  // Function is called first on GLFW_PRESS.
  if (action== GLFW_REPEAT) {
    switch (key) {
    case GLFW_KEY_ESCAPE:
      quit(window);
      break;
    case GLFW_KEY_R:
      player.x = 0;
      player.y = 40;
      player.z = 0;
      break;
    case GLFW_KEY_H:
      fc=2;
      world.cam.eyex = 60;
      world.cam.eyey = 60;
      world.cam.eyez = 60;
      break;
    case GLFW_KEY_G:
      fc=1;
      break;
    case GLFW_KEY_F:
      world.cam.eyex = -80;
      world.cam.eyey = 100;
      world.cam.eyez = 0;
      fc=0;
      break;
    case GLFW_KEY_Y:
      world.cam.eyex = player.x - 20;
      world.cam.eyey = player.y + 20;
      world.cam.eyez = player.z;
      world.cam.targetx = player.x;
      world.cam.targety = player.y;
      world.cam.targetz = player.z;
      fc=3;
      break;
    case GLFW_KEY_E:
      player.rotation_angle -= 10;
      break;
    case GLFW_KEY_Q:
      player.rotation_angle += 10;
      break;
    case GLFW_KEY_P:
      world.cam.rotation_angle += 1;
      break;
    case GLFW_KEY_O:
      world.cam.rotation_angle -= 1;
      break;
    case GLFW_KEY_8:
      world.cam.elevation_angle += 1;
      break;
    case GLFW_KEY_2:
      world.cam.elevation_angle -= 1;
      break;
    case GLFW_KEY_Z:
      world.cam.eyex = 0;
      world.cam.eyey = 60;
      world.cam.eyez = -30;
      fc=0;
      break;
    case GLFW_KEY_T:
      world.cam.eyex = 60;
      world.cam.eyey = 400;
      world.cam.eyez = 60;
      fc=4;
      break;
    // case GLFW_KEY_UP:
    //   world.cam.eyex += 1.0*cos(-world.cam.rotation_angle*M_PI/180.0);
    //   world.cam.eyez += 1.0*sin(-world.cam.rotation_angle*M_PI/180.0);
    //   world.cam.eyey += 1.0*sin(-world.cam.elevation_angle*M_PI/180.0);
    //   break;
    // case GLFW_KEY_DOWN:
    //   world.cam.eyex -= 1.0*cos(-world.cam.rotation_angle*M_PI/180.0);
    //   world.cam.eyez -= 1.0*sin(-world.cam.rotation_angle*M_PI/180.0);
    //   world.cam.eyey -= 1.0*sin(-world.cam.elevation_angle*M_PI/180.0);
    //   break;
    // case GLFW_KEY_RIGHT:
    //   world.cam.eyex -= 1.0*sin(-world.cam.rotation_angle*M_PI/180.0);
    //   world.cam.eyez += 1.0*cos(-world.cam.rotation_angle*M_PI/180.0);
    //   break;
    // case GLFW_KEY_LEFT:
    //   world.cam.eyex += 1.0*sin(-world.cam.rotation_angle*M_PI/180.0);
    //   world.cam.eyez -= 1.0*cos(-world.cam.rotation_angle*M_PI/180.0);
    //   break;
    default:
      break;
    }
  }
  
  if (action == GLFW_PRESS) {
    switch (key) {
    case GLFW_KEY_ESCAPE:
      quit(window);
      break;
    case GLFW_KEY_W:
      player.vx = player.speed*cos(-player.rotation_angle*M_PI/180.0);
      player.vz = player.speed*sin(-player.rotation_angle*M_PI/180.0);
      break;
    case GLFW_KEY_R:
      player.Restart();
      break;
    case GLFW_KEY_M:
      player.speed = 2.0;
      break;
    case GLFW_KEY_N:
      player.speed = 1.0;
      break;
    case GLFW_KEY_S:
      player.vx = -player.speed*cos(-player.rotation_angle*M_PI/180.0);
      player.vz = -player.speed*sin(-player.rotation_angle*M_PI/180.0);
      break;
    case GLFW_KEY_D:
      player.vx = -player.speed*sin(-player.rotation_angle*M_PI/180.0);
      player.vz = player.speed*cos(-player.rotation_angle*M_PI/180.0);
      break;
    case GLFW_KEY_A:
      player.vx = player.speed*sin(-player.rotation_angle*M_PI/180.0);
      player.vz = -player.speed*cos(-player.rotation_angle*M_PI/180.0);
      break;
    case GLFW_KEY_UP:
      world.cam.vx = 1.0;
      world.cam.vz = 1.0;
      world.cam.vy = 1.0;
      world.cam.strafe = -1.0;
      break;
    case GLFW_KEY_DOWN:
      world.cam.vx = -1.0;
      world.cam.vz = -1.0;
      world.cam.vy = -1.0;
      world.cam.strafe = -1.0;
      break;
    case GLFW_KEY_RIGHT:
      world.cam.vx = -1.0;
      world.cam.vz = 1.0;
      world.cam.vy = 0;
      world.cam.strafe = 1.0;
      break;
    case GLFW_KEY_LEFT:
      world.cam.vx = 1.0;
      world.cam.vz = -1.0;
      world.cam.vy = 0;
      world.cam.strafe = 1.0;
      break;
    default:
      break;
    }
  }

  if (action == GLFW_RELEASE) {
    switch (key) {
    case GLFW_KEY_ESCAPE:
      quit(window);
      break;
    case GLFW_KEY_W:
      player.vx = 0;
      player.vz = 0;
      break;
    case GLFW_KEY_S:
      player.vx = 0;
      player.vz = 0;
      break;
    case GLFW_KEY_D:
      player.vx = 0;
      player.vz = 0;
      break;
    case GLFW_KEY_A:
      player.vx = 0;
      player.vz = 0;
      break;
    case GLFW_KEY_UP:
      world.cam.vx = 0;
      world.cam.vy = 0;
      world.cam.vz = 0;
      break;
    case GLFW_KEY_DOWN:
      world.cam.vx = 0;
      world.cam.vy = 0;
      world.cam.vz = 0;
      break;
    case GLFW_KEY_RIGHT:
      world.cam.vx = 0;
      world.cam.vy = 0;
      world.cam.vz = 0;
      break;
    case GLFW_KEY_LEFT:
      world.cam.vx = 0;
      world.cam.vy = 0;
      world.cam.vz = 0;
      break;
    default:
      break;
    }
  }
}

/* Executed for character input (like in text boxes) */
void keyboardChar (GLFWwindow* window, unsigned int key)
{
  // cout<<key<<endl;
  switch (key) {
    break;
  case GLFW_KEY_SPACE:
    if(player.state == 1)
      {
	player.vy = 2.0;
	player.state = -1;
      }
    break;
  default:
    break;
  }
}

/* Executed when a mouse button is pressed/released */
void mouseButton (GLFWwindow* window, int button, int action, int mods)
{
  switch (button) {
  case GLFW_MOUSE_BUTTON_LEFT:
    if (action == GLFW_RELEASE)
      break;
  case GLFW_MOUSE_BUTTON_RIGHT:
    if (action == GLFW_RELEASE) {
    }
    break;
  default:
    break;
  }
}
