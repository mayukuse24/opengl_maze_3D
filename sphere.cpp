#include <iostream>
#include "initial.h"
#include "sphere.h"

using namespace std;

void Sphere::CreateObject(float xcoord,float ycoord,float zcoord,float radius,float red=0,float green=0,float blue=0)
{
  this->x = xcoord;
  this->y = ycoord;
  this->z = zcoord;
      
  GLfloat vertex_buffer_data[406100];
  GLfloat color_buffer_data[406100];

  int i,j,k,r=radius;
  k=0;
  vertex_buffer_data[0]=0;
  vertex_buffer_data[1]=0;
  vertex_buffer_data[2]=0;

  for(j=-90;j<=90;j++)
    {
      for(i=-180;i<=180;i++)
	{
	  vertex_buffer_data[k] = r*cos((i*M_PI)/180.0)*cos((j*M_PI)/180.0);
	  color_buffer_data[k++] = red;
	  vertex_buffer_data[k] = r*sin((i*M_PI)/180.0)*cos((j*M_PI)/180.0);
	  color_buffer_data[k++] = blue;
	  vertex_buffer_data[k] = r*sin((j*M_PI)/180.0);
	  color_buffer_data[k++] = green;

	}
    }
  cout<<k<<endl;
  this->mass = 5.0;
  this->radius = r;
  this->obj = create3DObject(GL_TRIANGLE_FAN, 64800, vertex_buffer_data, color_buffer_data, GL_FILL,NULL);
  
}

void Sphere::DrawObject(glm::mat4 VP, struct GLMatrices Matrices)
{
  glUseProgram(programID);
  Matrices.model = glm::mat4(1.0f);
  glm::mat4 MVP;
  glm::mat4 translateSphere = glm::translate (glm::vec3(this->x, this->y, this->z)); // glTranslatef
  glm::mat4 SphereTransform = translateSphere;
  Matrices.model *= SphereTransform;
  MVP = VP * Matrices.model; // MVP = p * V * M
  glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
  draw3DObject(this->obj);
}
