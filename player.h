#ifndef PLAYER_H
#define PLAYER_H

#include "sphere.h"

class Hero : public Sphere
{
 public:
  float rotation_angle = 0;
  float speed=1.0;
  void DrawObject(glm::mat4 , struct GLMatrices);
  void Restart();
};

extern Hero player;

#endif
