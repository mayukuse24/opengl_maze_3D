#include "ballcollision.h"
#include <iostream>

using namespace std;

void Reaction(Sphere &one,glm::vec3 poc,float dotprodx,float dotprody,float dotprodz)
{

  // cout<<"dotwithx "<<dotprodx<<endl;
  // cout<<"dotwithy "<<dotprody<<endl;
  // cout<<"dotwithz "<<dotprodz<<endl;

  if(dotprody > 0)
    {
      cout<<"top surface"<<endl;
      one.y = poc.y + one.radius ;
      if(one.state != -1)
	one.vy = 0;
      one.state=1;
    }
  else if(dotprody<0)
    {
      cout<<"bottom surface"<<endl;
      one.y = poc.y - one.radius ;
    }
  else if(dotprodx > 0)
    {
      cout<<" back "<<endl;
      one.x  = poc.x + one.radius  ;
      
    }
  else if(dotprodx < 0)
    {
      cout<<" front "<<endl;
      one.x = poc.x - one.radius  ;
    }
  else if(dotprodz > 0)
    {
      cout<<"right surface"<<endl;
      one.z = poc.z + one.radius ;
    }
  else if(dotprodz < 0)
    {
      cout<<"left surface"<<endl;
      one.z = poc.z - one.radius ;
    }

  // if(lowerlimit < one.vy && one.vy < upperlimit)
  //one.vy = 0;
}

bool Collision(Sphere &one, Cube &two)
{
  glm::vec3 centre(one.x,one.y,one.z);
  glm::vec3 AABB_centre(two.x,two.y,two.z);
  glm::vec3 AABB_half_size(two.length/2,two.height/2,two.width/2);
  
  glm::vec3 difference = centre - AABB_centre;
  glm::vec3 clamped = glm::clamp(difference, -AABB_half_size ,AABB_half_size);

  glm::vec3 poc = AABB_centre + clamped;
  difference = poc - centre;
  
  glm::vec3 centre_to_poc = centre - poc;
  glm::vec3 yaxis(0,1.0,0);
  glm::vec3 xaxis(1.0,0,0);
  glm::vec3 zaxis(0,0,1.0);
  
  float dotprody= glm::dot(centre_to_poc,yaxis);
  float dotprodx= glm::dot(centre_to_poc,xaxis);
  float dotprodz= glm::dot(centre_to_poc,zaxis);
  
  if(glm::length(difference) < one.radius)
    {
      //one.first_collision = 1;
      
      Reaction(one,poc,dotprodx,dotprody,dotprodz);
      cout<<two.height<<endl;
      return true;
    }
  else
    {
      //one.state = 1;
    }
    
  return false;
}


