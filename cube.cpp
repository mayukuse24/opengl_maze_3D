#include "initial.h"
#include "cube.h"

void Cube::CreateObject(float xcoord,float ycoord,float zcoord,float lenval,float widlval,float heival,int textureid,float red=0,float green=0,float blue=0)
{

  this->width = widlval;
  this->length = lenval;
  this->height = heival;
  
  this->x = xcoord;
  this->y = ycoord + this->height/2;
  this->z = zcoord;
  this->mass = 10;


  GLfloat perpendicular_buffer_data[ ] = {

    // Left face
    0.0f,0.0f,-1.0f,
    0.0f,0.0f,-1.0f,
    0.0f,0.0f,-1.0f,

    0.0f,0.0f,-1.0f,
    0.0f,0.0f,-1.0f,
    0.0f,0.0f,-1.0f,

    // Front face
    1.0f,0.0f,0.0f,
    1.0f,0.0f,0.0f,
    1.0f,0.0f,0.0f,

    1.0f,0.0f,0.0f,
    1.0f,0.0f,0.0f,
    1.0f,0.0f,0.0f,

    // Topface
    0.0f,1.0f,0.0f, // +ve yaxis
    0.0f,1.0f,0.0f, // +ve yaxis
    0.0f,1.0f,0.0f, // +ve yaxis
    
    0.0f,1.0f,0.0f, // +ve yaxis
    0.0f,1.0f,0.0f, // +ve yaxis
    0.0f,1.0f,0.0f, // +ve yaxis

    // Right face
    0.0f,0.0f,1.0f,
    0.0f,0.0f,1.0f,
    0.0f,0.0f,1.0f,

    0.0f,0.0f,1.0f,
    0.0f,0.0f,1.0f,
    0.0f,0.0f,1.0f,

    
    // Back face
    -1.0f,0.0f,0.0f,
    -1.0f,0.0f,0.0f,
    -1.0f,0.0f,0.0f,

    -1.0f,0.0f,0.0f,
    -1.0f,0.0f,0.0f,
    -1.0f,0.0f,0.0f,
        
    // Bottom face
    0.0f,-1.0f,0.0f, // -ve yaxis
    0.0f,-1.0f,0.0f, // -ve yaxis
    0.0f,-1.0f,0.0f, // -ve yaxis

    0.0f,-1.0f,0.0f, // -ve yaxis
    0.0f,-1.0f,0.0f, // -ve yaxis
    0.0f,-1.0f,0.0f, // -ve yaxis
    
  };
  
	
  GLfloat vertex_buffer_data [] = {

    //left face
    -this->length/2,this->height/2,this->width/2, // vertex 1
    -this->length/2,-this->height/2,this->width/2, // vertex 2
    -this->length/2,-this->height/2,-this->width/2, // vertex 3

    -this->length/2,this->height/2,this->width/2, // vertex 1
    -this->length/2,-this->height/2,-this->width/2, // vertex 3
    -this->length/2,this->height/2,-this->width/2, //vertex 4

    //front face
    -this->length/2,this->height/2,this->width/2, // vertex 1
    this->length/2,this->height/2,this->width/2, // vertex 5
    -this->length/2,-this->height/2,this->width/2, // vertex 2

    this->length/2,this->height/2,this->width/2, // vertex 5
    -this->length/2,-this->height/2,this->width/2, // vertex 2
    this->length/2,-this->height/2,this->width/2, //vertex 6
    
    //top face
    -this->length/2,this->height/2,this->width/2, // vertex 1
    this->length/2,this->height/2,this->width/2, // vertex 5
    -this->length/2,this->height/2,-this->width/2, //vertex 4
    
    this->length/2,this->height/2,this->width/2, // vertex 5
    -this->length/2,this->height/2,-this->width/2, //vertex 4
    this->length/2,this->height/2,-this->width/2, // vertex 7

    //right face
    this->length/2,this->height/2,this->width/2, // vertex 5
    this->length/2,-this->height/2,this->width/2, //vertex 6
    this->length/2,this->height/2,-this->width/2, // vertex 7

    this->length/2,-this->height/2,this->width/2, //vertex 6
    this->length/2,this->height/2,-this->width/2, // vertex 7
    this->length/2,-this->height/2,-this->width/2, //vertex 8

    //Back face
    -this->length/2,-this->height/2,-this->width/2, // vertex 3
    -this->length/2,this->height/2,-this->width/2, //vertex 4
    this->length/2,this->height/2,-this->width/2, // vertex 7

    -this->length/2,-this->height/2,-this->width/2, // vertex 3
    this->length/2,-this->height/2,-this->width/2, //vertex 8
    this->length/2,this->height/2,-this->width/2, // vertex 7

    //Bottom face
    -this->length/2,-this->height/2,-this->width/2, // vertex 3
    this->length/2,-this->height/2,-this->width/2, //vertex 8
    this->length/2,-this->height/2,this->width/2, //vertex 6

    -this->length/2,-this->height/2,-this->width/2, // vertex 3
    -this->length/2,-this->height/2,this->width/2, // vertex 2
    this->length/2,-this->height/2,this->width/2 //vertex 6
     
  };

  GLfloat color_buffer_data [] = {
    
    red,green,blue, // color 1
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue, // color 1
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue, // color 1
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue,

    red,green,blue,
    red,green,blue,
    red,green,blue

  };


  GLfloat texture_buffer_data [] = {
   0,1, 
   1,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 
   
   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 
 };
  // create3DTexturedObject creates and returns a handle to a VAO that can be used later
  
  if(textureid==-1)
    this->obj = create3DObject(GL_TRIANGLES, 36, vertex_buffer_data, color_buffer_data, GL_FILL, perpendicular_buffer_data);
  else 
    this->obj = create3DTexturedObject(GL_TRIANGLES, 36, vertex_buffer_data, texture_buffer_data,textureid, GL_FILL);
  
}

void Cube::DrawObject(glm::mat4 VP, struct GLMatrices Matrices)
{
  glUseProgram(programID);
  Matrices.model = glm::mat4(1.0f);
  glm::mat4 MVP;
  glm::mat4 translateCube = glm::translate (glm::vec3(this->x, this->y, this->z)); // glTranslatef
  glm::mat4 CubeTransform = translateCube;
  Matrices.model *= CubeTransform;
  MVP = VP * Matrices.model; // MVP = p * V * M
  glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
  draw3DObject(this->obj);
}

void Cube::DrawTexturedObject(glm::mat4 VP, struct GLMatrices Matrices)
{
  glUseProgram(textureProgramID);
  Matrices.model = glm::mat4(1.0f);
  glm::mat4 MVP;
  glm::mat4 translateCube = glm::translate (glm::vec3(this->x, this->y, this->z)); // glTranslatef
  glm::mat4 CubeTransform = translateCube;
  Matrices.model *= CubeTransform;
  MVP = VP * Matrices.model; // MVP = p * V * M
  glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
  glUniform1i(glGetUniformLocation(textureProgramID, "texSampler"), 0);
  draw3DTexturedObject(this->obj);
}


