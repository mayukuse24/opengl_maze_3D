#ifndef COLLISION_H
#define COLLISION_H

#include "initial.h"
#include "cube.h"

bool CheckCollision(Cube&,Cube&);
void CollisionReaction(Cube,Cube);
#endif
