all: sample2D

sample2D: main.cpp glad.c
	g++ -g -std=c++11 -o tempo main.cpp ballcollision.cpp game.cpp object.cpp player.cpp sphere.cpp movable.cpp keyboard.cpp \
cube.cpp glad.c initial.cpp -lGL -lglfw -ldl -lSOIL -lftgl -L/usr/local/lib -I/usr/include -I/usr/include/freetype2 \
./libIrrKlang.so ./ikpMP3.so -pthread
clean:
	rm sample2D
