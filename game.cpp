#include "game.h"
#include <iostream>

using namespace std;

void Game::GenerateMap(glm::mat4 VP, struct GLMatrices Matrices,Cube surface[],Cube raised[],Cube Groundtiles[],Movable vertical[],Cube finish[])
{
  
  for(int a=0;a<this->surf_count;a++)  
    surface[a].DrawTexturedObject(VP,Matrices);

  for(int a=0;a<this->raised_count;a++)  
    raised[a].DrawTexturedObject(VP,Matrices);

  for(int a=0;a<this->vert_count;a++)  
    vertical[a].DrawTexturedObject(VP,Matrices);

  for(int a=0;a<this->ground_count;a++)  
    Groundtiles[a].DrawObject(VP,Matrices);

  for(int a=0;a<this->finish_count;a++)  
    finish[a].DrawObject(VP,Matrices);

}

void Game::CreateMap(Cube surface[],Cube raised[],Movable vertical[],Cube Groundtiles[],Cube finish[],GLuint texarr[])
{
  this->surf_count=0;
  this->raised_count=0;
  this->vert_count=0;
  this->ground_count=0;
  this->finish_count=0;
  
  float surfacewidth=10;
  float surfacelength=10;
  float row=0,column=0;
  
  for(int c=0;c<this->levels;c++)
    {
      row=0;column=0;

      for(int a=0;a<this->plot_rows;a++)
	{
	  for(int b=0;this->mapplot2d[a][b][c];b++)
	    {
	      if(this->mapplot2d[a][b][c] == 1 )
		{
		  surface[this->surf_count++].CreateObject(column,c*30,row,10,10,30,texarr[1],0,0,0);
		  Groundtiles[this->ground_count++].CreateObject(column,c*30+30.1,row,10,10,0,-1,0.2,0.2,0.2);
		}
	      else if(this->mapplot2d[a][b][c] == 3)
		{
		  raised[this->raised_count++].CreateObject(column,c*30,row,10,10,40,texarr[0],1,1,0.5);
		  Groundtiles[this->ground_count++].CreateObject(column,c*30+30.1,row,10,10,0,-1,0.2,0.2,0.2);
		}
	      else if(this->mapplot2d[a][b][c] == 4)
		{
		  vertical[this->vert_count].CreateObject(column,c*30+20,row,10,10,10,texarr[2],0.5,1,0.2);
		  vertical[this->vert_count++].Range(glm::vec3(column,c*30+20,row),glm::vec3(column,c*30+60,row),0.2);
		}
	      else if(this->mapplot2d[a][b][c] == 9)
		{
		  surface[this->surf_count++].CreateObject(column,c*30,row,10,10,30,texarr[1],0,0,0);
		  finish[this->finish_count++].CreateObject(column,c*30+30.1,row,10,10,0,-1,0.4,0.4,0.4);
		}
	      column += surfacelength;
	    }
	  row += surfacewidth;
	  column=0;
	}
    }    
}


void Sky::CreateObject(float xcoord,float ycoord,float zcoord,float lenval,float widlval,float heival,int textureid,float red=0,float green=0,float blue=0)
{

  this->width = widlval;
  this->length = lenval;
  this->height = heival;
  
  this->x = xcoord;
  this->y = ycoord + this->height/2;
  this->z = zcoord;
  this->mass = 10;
  
  GLfloat vertex_buffer_data [] = {

    //back face
    -length/2,height/2,width/2, // vertex 1
    -length/2,-height/2,width/2, // vertex 2
    -length/2,-height/2,-width/2, // vertex 3

    -length/2,height/2,width/2, // vertex 1
    -length/2,-height/2,-width/2, // vertex 3
    -length/2,height/2,-width/2, //vertex 4

    //right face
    -length/2,height/2,width/2, // vertex 1
    length/2,height/2,width/2, // vertex 5
    -length/2,-height/2,width/2, // vertex 2

    length/2,height/2,width/2, // vertex 5
    -length/2,-height/2,width/2, // vertex 2
    length/2,-height/2,width/2, //vertex 6
    
    //top face
    -length/2,height/2,width/2, // vertex 1
    length/2,height/2,width/2, // vertex 5
    -length/2,height/2,-width/2, //vertex 4
    
    length/2,height/2,width/2, // vertex 5
    -length/2,height/2,-width/2, //vertex 4
    length/2,height/2,-width/2, // vertex 7

    //front face
    
    length/2,height/2,width/2, // vertex 5
    length/2,-height/2,width/2, //vertex 6
    length/2,height/2,-width/2, // vertex 7

    length/2,-height/2,width/2, //vertex 6
    length/2,height/2,-width/2, // vertex 7
    length/2,-height/2,-width/2, //vertex 8

    //left face
    -length/2,-height/2,-width/2, // vertex 3
    -length/2,height/2,-width/2, //vertex 4
    length/2,height/2,-width/2, // vertex 7

    -length/2,-height/2,-width/2, // vertex 3
    length/2,-height/2,-width/2, //vertex 8
    length/2,height/2,-width/2, // vertex 7

    //Bottom face
    -length/2,-height/2,-width/2, // vertex 3
    length/2,-height/2,-width/2, //vertex 8
    length/2,-height/2,width/2, //vertex 6

    -length/2,-height/2,-width/2, // vertex 3
    -length/2,-height/2,width/2, // vertex 2
    length/2,-height/2,width/2 //vertex 6
     
  };

  GLfloat color_buffer_data [] = {

    // back face
    0,0,0,
    1,1,1,
    1,1,1,

    0,0,0,
    1,1,1,
    0,0,0,
            
    //right face
    0,0,0,
    0,0,0,
    1,1,1,

    0,0,0,
    1,1,1,
    1,1,1,
    
    //top face
    0,0,0,
    0,0,0,
    0,0,0,

    0,0,0,
    0,0,0,
    0,0,0,

    //front face
    0,0,0,
    1,1,1,
    0,0,0,

    1,1,1,
    0,0,0,
    1,1,1,
    
    // left face
    1,1,1,
    0,0,0,
    0,0,0,

    1,1,1,
    1,1,1,
    0,0,0,
    
    // bottom face
    1,1,1,
    1,1,1,
    1,1,1,

    1,1,1,
    1,1,1,
    1,1,1
    
  };


  GLfloat texture_buffer_data [] = {
   0,1, 
   1,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 
   
   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 

   1,1, 
   0,1, 
   1,0, 

   0,1,  
   1,0, 
   0,0, 
 };
  // create3DTexturedObject creates and returns a handle to a VAO that can be used later
  this->obj = create3DObject(GL_TRIANGLES, 36, vertex_buffer_data, color_buffer_data, GL_FILL,NULL);
  
}
