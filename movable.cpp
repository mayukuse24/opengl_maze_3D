#include "initial.h"
#include "movable.h"

void Movable::Range(glm::vec3 start,glm::vec3 end,float speed)
{
  this->A = start;
  this->B = end;

  if(A.x != B.x)
    this->vx = speed;

  if(A.y != B.y)
    this->vy = speed;

  if(A.z != B.z)
    this->vz = speed;
}
