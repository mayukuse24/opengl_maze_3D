#ifndef CUBE_H
#define CUBE_H

#include "object.h"

class Cube : public Object
{
 public:
  //float x,y,z,vx,vy,vz,mass;
  float width,height,length;
  //VAO *obj;
  

  void CreateObject(float,float,float,float,float,float,int,float,float,float);
  void DrawObject(glm::mat4,struct GLMatrices);
  void DrawTexturedObject(glm::mat4, struct GLMatrices);
};

#endif
