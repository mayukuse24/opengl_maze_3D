#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "initial.h"

void keyboard (GLFWwindow* , int , int , int , int );
void keyboardChar (GLFWwindow* , unsigned int );
void mouseButton (GLFWwindow* , int, int, int);
void quit(GLFWwindow *);
  
extern float targetx,targety,targetz;
extern int fc;

#endif
