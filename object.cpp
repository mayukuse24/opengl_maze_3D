#include "object.h"

void Object::ApplyVelocity()
{
  this->x += this->vx;
  this->y += this->vy;
  this->z += this->vz;
}
