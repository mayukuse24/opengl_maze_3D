#ifndef SPHERE_H
#define SPHERE_H

#include "object.h"

class Sphere : public Object
{
 public:
  float radius;
  int state = 1;

  void CreateObject(float,float,float,float,float,float,float);
  void DrawObject(glm::mat4,struct GLMatrices);
  //void DrawTexturedObject(glm::mat4, struct GLMatrices);
  //void ApplyVelocity();
};

#endif
