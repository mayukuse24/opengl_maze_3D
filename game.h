#ifndef GAME_H
#define GAME_H

#include "initial.h"
#include "cube.h"
#include "movable.h"

class Camera
{
 public:
  float eyex,eyey,eyez;
  float vx,vy,vz,strafe;
  float targetx,targety,targetz;
  float rotation_angle=0,elevation_angle=0;
  
};

class Sky : public Cube
{
 public:
  
  void CreateObject(float,float,float,float,float,float,int,float,float,float);
};

class Game : public Object
{
 public:
  int mapplot2d[100][100][10],levels=4,plot_rows;
  int vert_count,raised_count,surf_count,ground_count,finish_count;
  Camera cam; 
  float gravity;
  Sky atmo;
  
  void GenerateMap(glm::mat4, struct GLMatrices,Cube [],Cube [],Cube [],Movable [],Cube []);

  void CreateMap(Cube [],Cube [],Movable [],Cube [],Cube [],GLuint []);
};


extern Game world;
#endif
