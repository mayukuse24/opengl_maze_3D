#include <iostream>
#include "cube.h"
#include "movable.h"
#include "stdlib.h"

using namespace std;

void CollisionReaction(Cube &one,Cube &two)
{

  float newvx1 = (one.vx * (one.mass - two.mass) + (2 * two.mass * two.vx)) / (one.mass + two.mass);
  float newvy1 = (one.vy * (one.mass - two.mass) + (2 * two.mass * two.vy)) / (one.mass + two.mass);
  float newvz1 = (one.vz * (one.mass - two.mass) + (2 * two.mass * two.vz)) / (one.mass + two.mass);
  
  float newvx2 = (two.vx * (two.mass - one.mass) + (2 * one.mass * one.vx)) / (one.mass + two.mass);
  float newvy2 = (two.vy * (two.mass - one.mass) + (2 * one.mass * one.vy)) / (one.mass + two.mass);
  float newvz2 = (two.vz * (two.mass - one.mass) + (2 * one.mass * one.vz)) / (one.mass + two.mass);

  one.vx = newvx1;
  one.vy = newvy1;
  one.vx = newvx1;
  
  two.vx = newvx2;
  two.vy = newvy2;
  two.vz = newvz2;    

  cout<<"one"<<one.vx<<one.vy<<one.vz<<endl;
  cout<<"two"<<two.vx<<two.vy<<two.vz<<endl;
}


bool CheckCollision(Cube &one,Cube &two)
{
  if(abs(one.x - two.x) < one.length/2 + two.length/2)
   {
     if(abs(one.y - two.y) < one.height/2 + two.height/2)
      {
	if(abs(one.z - two.z) < one.width/2 + two.width/2)
          {
	    CollisionReaction(one,two);
	    one.ApplyVelocity();
	    two.ApplyVelocity();
	    return true;
	  }
      }
   }

  return false;
  //cout<<"No collision"<<endl;
}
