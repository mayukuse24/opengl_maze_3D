#ifndef MOVABLE_H
#define MOVABLE_H

#include "initial.h"
#include "cube.h"

class Movable : public Cube
{
 public:
  glm::vec3 A;
  glm::vec3 B;
  
  void Range(glm::vec3, glm::vec3,float);
};

#endif
