#ifndef OBJECT_H
#define OBJECT_H

#include "initial.h"

class Object
{
 public:
  float x,y,z,vx,vy,vz,mass;
  VAO *obj;
  void ApplyVelocity();
  
};

#endif
