#ifndef BALLCOLLISION_H
#define BALLCOLLISION_H

#include "initial.h"
#include "cube.h"
#include "sphere.h"

bool Collision(Sphere&,Cube&);
void Reaction(Sphere&,glm::vec3,float,float,float);
#endif
