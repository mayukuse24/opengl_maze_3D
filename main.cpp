#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <irrklang/irrKlang.h>

#include <glad/glad.h>
#include <FTGL/ftgl.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shape.h"
#include "initial.h"
#include "object.h"
#include "cube.h"
#include "keyboard.h"
#include "collision.h"
#include "ballcollision.h"
#include "movable.h"
#include "sphere.h"
#include "player.h"
#include "game.h"

#define NavyBluered 0.137255
#define NavyBluegreen 0.137255 
#define NavyBlueblue 0.556863
using namespace std;
using namespace irrklang;

typedef struct VAO VAO;
struct GLMatrices Matrices;
struct FTGLFont GL3Font;

Cube surface[1000],Groundtiles[1000],raised[1000],start,finish[1000];
Hero player;
Movable horizontal,horizontal2,vertical[1000];
GLuint programID, fontProgramID, textureProgramID;
Game world;
GLfloat fov = 45.0f;
int fbwidth, fbheight;
/* Function to load Shaders - Use it as it is */

static void error_callback(int error, const char* description)
{
  cout << "Error: " << description << endl;
}

void getMap()
{
  ifstream in;
  char fname[100]="map1.txt";
  //cout<<strlen(fname)<<endl;
  
  string line;
  int b=0;

  for(int i=1;i<=world.levels;i++)
    {
      b=0;
      fname[3]=48 + i;
      in = ifstream(fname);
      cout<<fname<<endl;
      while(getline(in,line))
	{
	  for(int a=0;line[a];a++)
	    {
	      world.mapplot2d[b][a][i-1]=line[a]-48;
	    }
	  b++;
	}
      world.plot_rows = b;
      cout<<b<<endl;
      for(int c=0;c<b;c++)
	{
	  for(int a=0;world.mapplot2d[c][a][i-1];a++)
	    {
	      cout<<world.mapplot2d[c][a][i-1]<< " ";
	    }
	  cout<<endl;
	}

    }
}

void reshapeWindow (GLFWwindow* window, int width, int height)
{
  
  /* With Retina display on Mac OS X, GLFW's FramebufferSize
     is different from WindowSize */
  glfwGetFramebufferSize(window, &fbwidth, &fbheight);



  // sets the viewport of openGL renderer
  glViewport (0, 0, (GLsizei) fbwidth, (GLsizei) fbheight);

  // set the projection matrix as perspective
  /* glMatrixMode (GL_PROJECTION);
     glLoadIdentity ();
     gluPerspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1, 500.0); */
  // Store the projection matrix in a variable for future use
  // Perspective projection for 3D views
  Matrices.projection = glm::perspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1f, 10000.0f);

  // Ortho projection for 2D views
  //Matrices.projection = glm::ortho(-100.0f, 100.0f, -100.0f, 100.0f, 0.1f, 500.0f);
}

VAO *line;
// Creates the triangle object used in this sample code
// Creates the rectangle object used in this sample code
void createAxis()
{
    // GL3 accepts only Triangles. Quads are not supported
  static const GLfloat vertex_buffer_data [] = {
    -300,0,0, // vertex 1
    300,0,0, // vertex 2
    
    0, -300,0, // vertex 3
    0, 300,0, // vertex 3
    
    0, 0,-300, // vertex 4
    0, 0, 300  // vertex 1
  };

  static const GLfloat color_buffer_data [] = {
    1,0,0, // color 1
    1,0,0, // color 2
    
    0,1,0, // color 3
    0,1,0, // color 3
    
    0,0,1,
    0,0,1  // color 1
  };

  // create3DTexturedObject creates and returns a handle to a VAO that can be used later
  line = create3DObject(GL_LINES, 6, vertex_buffer_data, color_buffer_data, GL_FILL,NULL);
  
}

float camera_rotation_angle = 90;

/* Render the scene with openGL */
/* Edit this function according to your assignment */

void object_patrol(Movable &one)
{
  if(one.x < one.A.x || one.x > one.B.x)
    one.vx *= -1;

  if(one.y < one.A.y || one.y > one.B.y)
    one.vy *= -1;

  if(one.z < one.A.z || one.z > one.B.z)
    one.vz *= -1;

  one.ApplyVelocity();
}

void draw ()
{
  // clear the color and depth in the frame buffer
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glPointSize(5.0f);
  // use the loaded shader program
  // Don't change unless you know what you are doing
  glUseProgram (programID);

  // Eye - Location of camera. Don't change unless you are sure!!
  
  glm::vec3 eye(world.cam.eyex,world.cam.eyey,world.cam.eyez);
  // Target - Where is the camera looking at.  Don't change unless you are sure!!
  glm::vec3 target(world.cam.targetx,world.cam.targety,world.cam.targetz);
  // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
  glm::vec3 up (0, 1, 0);

  Matrices.projection = glm::perspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1f, 10000.0f);
  // Compute Camera matrix (view)
  // Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
  //  Don't change unless you are sure!!
  // static float c = 0;
  // c++;
  //Matrices.view = glm::lookAt(glm::vec3(world.cam.eyex,0,3), glm::vec3(0,0,0), glm::vec3(sinf(c*M_PI/180.0),3*cosf(c*M_PI/180.0),0)); // Fixed camera for 2D (ortho) in XY plane \

  if(fc==1)
    {
      world.cam.eyex = player.x;
      world.cam.eyey = player.y + 5;
      world.cam.eyez = player.z;
      world.cam.targetx = world.cam.eyex + (5 * cos(-player.rotation_angle*M_PI/180.0));
      world.cam.targety = world.cam.eyey;
      world.cam.targetz = world.cam.eyez + (5 * sin(-player.rotation_angle*M_PI/180.0));
    }
  else if(fc==0)
    {
      world.cam.targetx = player.x;
      world.cam.targety = player.y;
      world.cam.targetz = player.z;
    }
  else if(fc==2)
    {
      world.cam.targetx = world.cam.eyex + (5 * cos(-world.cam.rotation_angle*M_PI/180.0));
      world.cam.targetz = world.cam.eyez + (5 * sin(-world.cam.rotation_angle*M_PI/180.0));
      world.cam.targety = world.cam.eyey + (5 * sin(-world.cam.elevation_angle*M_PI/180.0));
    }
  else if(fc==3)
    {
      world.cam.eyex = player.x - 20*cos(-player.rotation_angle*M_PI/180.0);;
      world.cam.eyey = player.y + 20;
      world.cam.eyez = player.z - 20*sin(-player.rotation_angle*M_PI/180.0);;
      world.cam.targetx = player.x;
      world.cam.targety = player.y;
      world.cam.targetz = player.z;
    }
  else if(fc==4)
    {
      world.cam.targetx = 70;
      world.cam.targety = 100;
      world.cam.targetz = 70;

    }
  
  Matrices.view = glm::lookAt(eye,target,up); // Fixed camera for 2D (ortho) in XY plane

  // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
  //  Don't change unless you are sure!!
  glm::mat4 VP = Matrices.projection * Matrices.view;
  
  world.GenerateMap(VP,Matrices,surface,raised,Groundtiles,vertical,finish);

  player.DrawObject(VP, Matrices);
  horizontal.DrawObject(VP,Matrices);
  horizontal2.DrawObject(VP,Matrices);
  world.atmo.DrawObject(VP,Matrices);
  start.DrawObject(VP, Matrices);
  // Send our transformation to the currently bound shader, in the "MVP" uniform
  // For each model you render, since the MVP will be different (at least the M part)
  //  Don't change unless you are sure!!
  glm::mat4 MVP;	// MVP = Projection * View * Model

  // Load identity to model matrix
  
  Matrices.model = glm::mat4(1.0f);

  /* Render your scene */
  glm::mat4 translateLine = glm::translate (glm::vec3(0.0f, 0.0f, 0.0f)); // glTranslatef
  glm::mat4 lineTransform = translateLine;
  Matrices.model *= lineTransform;
  MVP = VP * Matrices.model; // MVP = p * V * M
  glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
  draw3DObject(line);

  
  // Increment angles
  float increments = 1;

  // Render font on screen
  static int fontScale = 0;
  float fontScaleValue = 0.75 + 0.25*sinf(fontScale*M_PI/180.0f);
  glm::vec3 fontColor = getRGBfromHue (fontScale);

  // Use font Shaders for next part of code
  glUseProgram(fontProgramID);
  Matrices.view = glm::lookAt(glm::vec3(0,0,3), glm::vec3(0,0,0), glm::vec3(0,1,0)); // Fixed camera for 2D (ortho) in XY plane

  // Transform the text
  Matrices.model = glm::mat4(1.0f);
  glm::mat4 translateText = glm::translate(glm::vec3(-3,2,0));
  glm::mat4 scaleText = glm::scale(glm::vec3(fontScaleValue,fontScaleValue,fontScaleValue));
  Matrices.model *= (translateText * scaleText);
  MVP = Matrices.projection * Matrices.view * Matrices.model;
  // send font's MVP and font color to fond shaders
  glUniformMatrix4fv(GL3Font.fontMatrixID, 1, GL_FALSE, &MVP[0][0]);
  glUniform3fv(GL3Font.fontColorID, 1, &fontColor[0]);

  // Render font
  GL3Font.font->Render("Maze Runner ");

  //camera_rotation_angle++; // Simulating camera rotation
  
  player.ApplyVelocity();
  player.vy += world.gravity;
  object_patrol(horizontal);
  object_patrol(horizontal2);
  if(world.cam.strafe == -1)
    {
      world.cam.eyex += world.cam.vx*cos(-world.cam.rotation_angle*M_PI/180.0);
      world.cam.eyey += world.cam.vy*sin(-world.cam.elevation_angle*M_PI/180.0);
      world.cam.eyez += world.cam.vz*sin(-world.cam.rotation_angle*M_PI/180.0);
    }
  else
    {
      world.cam.eyex += world.cam.vx*sin(-world.cam.rotation_angle*M_PI/180.0);
      world.cam.eyey += world.cam.vy*sin(-world.cam.elevation_angle*M_PI/180.0);
      world.cam.eyez += world.cam.vz*cos(-world.cam.rotation_angle*M_PI/180.0);
      
    }
  
  for(int a=0;a<world.vert_count;a++)
    object_patrol(vertical[a]);

  if(player.y < -100)
    {
      player.Restart();
    }
}

void mouse_wheel_callback(GLFWwindow *window,double xpos, double ypos)
{
  cout<<ypos<<endl;
  cout<<fov<<endl;
  if(ypos==1 && fov<= 46.5)
    fov += 0.1;
  else if(ypos == -1 && fov >= 44.5)
    fov -= 0.1;
}

void cursor_pos_callback(GLFWwindow *window,double xpos, double ypos)
{
  //cout<<"xpos "<<xpos<<endl;
  //cout<<"ypos "<<ypos<<endl;
  if(fc==2)
    {
      if(xpos-300 < -1)
	{
	  world.cam.rotation_angle += 0.5;
	}
      else if(xpos-300 > 1)
	{
	  world.cam.rotation_angle -= 0.5;
	}

      if(ypos-300 < -1)
	{
	  world.cam.elevation_angle += 0.5;
	}
      else if(ypos-300 > 1)
	{
	  world.cam.elevation_angle -= 0.5;
	} 
    }
  else
    {
      if(xpos-300 < -1)
	{
	  player.rotation_angle += 0.5;
	}
      else if(xpos-300 > 1)
	{
	  player.rotation_angle -= 0.5;
	}
    }
}

/* Initialise glfw window, I/O callbacks and the renderer to use */
/* Nothing to Edit here */
GLFWwindow* initGLFW (int width, int height)
{
  GLFWwindow* window; // window desciptor/handle

  glfwSetErrorCallback(error_callback);
  if (!glfwInit()) {
    exit(EXIT_FAILURE);
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  window = glfwCreateWindow(width, height, "Sample OpenGL 3.3 Application", NULL, NULL);

  if (!window) {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(window);
  gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
  glfwSwapInterval( 1 );

  /* --- register callbacks with GLFW --- */

  /* Register function to handle window resizes */
  /* With Retina display on Mac OS X GLFW's FramebufferSize
     is different from WindowSize */
  glfwSetFramebufferSizeCallback(window, reshapeWindow);
  glfwSetWindowSizeCallback(window, reshapeWindow);

  /* Register function to handle window close */
  glfwSetWindowCloseCallback(window, quit);

  /* Register function to handle keyboard input */
  glfwSetKeyCallback(window, keyboard);      // general keyboard input
  glfwSetCharCallback(window, keyboardChar);  // simpler specific character handling

  /* Register function to handle mouse click */
  glfwSetMouseButtonCallback(window, mouseButton);  // mouse button clicks
  glfwSetScrollCallback(window,mouse_wheel_callback); 
  glfwSetCursorPosCallback(window, cursor_pos_callback);
  
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

  return window;
}

void initGL (GLFWwindow* window, int width, int height)
{
  // Load Textures
  // Enable Texture0 as current texture memory
  glActiveTexture(GL_TEXTURE0);
  // load an image file directly as a new OpenGL texture
  
  GLuint platformID = createTexture("tile_texture.jpg");
  // check for an error during the load process
  if(platformID == 0 )
    cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

  GLuint raisedID = createTexture("plat.jpg");
  // check for an error during the load process
  if(raisedID == 0 )
    cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

  GLuint elevateID = createTexture("elevate.png");
  // check for an error during the load process
  if(elevateID == 0 )
    cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

  // Create and compile our GLSL program from the texture shaders
  textureProgramID = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
  // Get a handle for our "MVP" uniform
  Matrices.TexMatrixID = glGetUniformLocation(textureProgramID, "MVP");
  
  /* Objects should be created before any other gl function and shaders */
  // Create the models

  //surface[a].CreateObject(x,y,z,length,width,height,textureid,red,green,blue);
  //CAREFUL COLORS CURRENTLY MIXED :P
  world.gravity = -0.1;
  world.cam.eyex = -20;
  world.cam.eyey = 50;
  world.cam.eyez = 0;
  GLuint texarr[] = {raisedID, platformID,elevateID};
  world.CreateMap(surface,raised,vertical,Groundtiles,finish,texarr);
    
  horizontal.CreateObject(40,surface[0].height,0,10,20,10,-1,0,1,0);
  horizontal.Range(glm::vec3(40,30,0),glm::vec3(40,30,100),0.5);

  horizontal2.CreateObject(120,surface[0].height,0,10,20,10,-1,0,1,0);
  horizontal2.Range(glm::vec3(120,30,0),glm::vec3(120,30,100),0.5);
  
  //player.CreateObject(0,35,0,5,5,5,-1,0,0,0);
  player.CreateObject(0,45,0,4,1,0,0);
  world.atmo.CreateObject(0,0,0,5000,5000,5000,-1,0.5,0.5,0.5);
  start.CreateObject(0, 30.1, 0, 10, 10, 0, -1, 0.8, 0.8, 0.8);
  createAxis();

  // Create and compile our GLSL program from the shaders
  programID = LoadShaders( "Sample_GL3.vert", "Sample_GL3.frag" );
  // Get a handle for our "MVP" uniform
  Matrices.MatrixID = glGetUniformLocation(programID, "MVP");

  reshapeWindow (window, width, height);

  // Background color of the scene
  glClearColor (0.196078 ,0.6 ,0.8, 0.0f); // R, G, B, A
  glClearDepth (1.0f);

  glEnable (GL_DEPTH_TEST);
  glDepthFunc (GL_LEQUAL);
  //glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Initialise FTGL stuff
  const char* fontfile = "arial.ttf";
  GL3Font.font = new FTExtrudeFont(fontfile); // 3D extrude style rendering

  if(GL3Font.font->Error())
    {
      cout << "Error: Could not load font `" << fontfile << "'" << endl;
      glfwTerminate();
      exit(EXIT_FAILURE);
    }

  // Create and compile our GLSL program from the font shaders
  fontProgramID = LoadShaders( "fontrender.vert", "fontrender.frag" );
  GLint fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform;
  fontVertexCoordAttrib = glGetAttribLocation(fontProgramID, "vertexPosition");
  fontVertexNormalAttrib = glGetAttribLocation(fontProgramID, "vertexNormal");
  fontVertexOffsetUniform = glGetUniformLocation(fontProgramID, "pen");
  GL3Font.fontMatrixID = glGetUniformLocation(fontProgramID, "MVP");
  GL3Font.fontColorID = glGetUniformLocation(fontProgramID, "fontColor");

  GL3Font.font->ShaderLocations(fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform);
  GL3Font.font->FaceSize(1);
  GL3Font.font->Depth(0);
  GL3Font.font->Outset(0, 0);
  GL3Font.font->CharMap(ft_encoding_unicode);

  cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
  cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
  cout << "VERSION: " << glGetString(GL_VERSION) << endl;
  cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

}
/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */


int main (int argc, char** argv)
{
  int width = 600;
  int height = 600;
  float oldx,oldy,oldz;
  GLFWwindow* window = initGLFW(width, height);

  fbwidth=width;
  fbheight=height;
  getMap();
  initGL (window, width, height);

  ISoundEngine *SoundEngine = createIrrKlangDevice();
  SoundEngine->play2D("theme.mp3", GL_TRUE);
  
  double last_update_time = glfwGetTime(), current_time;

  /* Draw in loop */
  while (!glfwWindowShouldClose(window)) {

    // OpenGL Draw commands
    glfwSetCursorPos (window, width/2, height/2);
    draw();
    
    // Swap Frame Buffer in double buffering
    glfwSwapBuffers(window);
    
    // Poll for Keyboard and mouse events
    glfwPollEvents();

    // Control based on time (Time based transformation like 5 degrees rotation every 0.5s)
    current_time = glfwGetTime(); // Time in seconds
    if ((current_time - last_update_time) >= 0.5) { // atleast 0.5s elapsed since last frame
      // do something every 0.5 seconds ..
      last_update_time = current_time;
    }
    
    for(int a=0;a<world.surf_count;a++)
    {
      Collision(player,surface[a]);
    }

    for(int a=0;a<world.raised_count;a++)
    {
      Collision(player,raised[a]);
    }

    for(int a=0;a<world.vert_count;a++)
    {
      Collision(player,vertical[a]);
    }

    Collision(player, horizontal);
    Collision(player, horizontal2);
  }

  glfwTerminate();
  exit(EXIT_SUCCESS);
}
